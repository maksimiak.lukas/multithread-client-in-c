# Multithread Client in C

Multithread Client in C is a simple cross-platform application that connects to a given server and performs simple tasks (it can receive and send information).
It is created for learning purposes only. If you connect to only one port you can send multiple messages. However, if you choose to send information to different ports on the same IP address, your message will be sent once (through all ports) and you will be able to establish new connection if required.

## Prerequisites

Make sure you have installed gcc: https://gcc.gnu.org/install/download.html


## Compiling and running


Linux
```bash
gcc client client.c -lpthread
./client ip_address port [Ports...]
```
Windows
```bash
gcc .\client.c -o client -w -lpthread -lws2_32
client.exe ip_address port [Ports...]
```

## Usage

Specify IP address and ports and communicate with the server.
You will be prompted to enter your message and asked whether you want to make a new connection with the server (when you connect to multiple ports). 

## Comments
This client is only a prototype of a fully functional client. It needs further polishing in order to be used in professional environment. 
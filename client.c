#ifdef _WIN32
#define _WIN32_WINNT 0x501
#include <winsock2.h>
#include <stdio.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <windows.h>
#include <pthread.h>
#include <limits.h>
#include <stdint.h>
#define bzero(b,len) (memset((b), '\0', (len)), (void) 0)
#define sleep(val) Sleep(val * 1000)


#else
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include<pthread.h>
#include <errno.h>
#include <limits.h>
#include <string.h>
#include <stdint.h>

#endif


#define MAX_BUFF_SIZE 200

//global for sake of simplicity
char *msg, *ip;
pthread_mutex_t mutex;
int CLIENT_NO = 0;

void *connection_handler(void *socket_desc);

char *read_line(void)
{
  int bufsize = MAX_BUFF_SIZE;
  int position = 0;
  char *buffer = malloc(sizeof(char) * bufsize);
  int c;

  if (!buffer) {
    fprintf(stderr, "allocation error\n");
    exit(EXIT_FAILURE);
  }

  while (1) {
    // Read a character
    c = getchar();

    if (c == EOF || c == '\n') {
      buffer[position] = '\0';
      return buffer;
    } else {
      buffer[position] = c;
    }
    position++;

    // If we have exceeded the buffer, reallocate
    if (position >= bufsize) {
      bufsize += MAX_BUFF_SIZE;
      buffer = realloc(buffer, bufsize);
      if (!buffer) {
        fprintf(stderr, "allocation error\n");
        exit(EXIT_FAILURE);
      }
    }
  }
}
int sendall(int s, char *buf, int *len)
{
    int total = 0;
    int bytesleft = *len;
    int n;

    while(total < *len) {
        n = send(s, buf+total, bytesleft, 0);
        if (n == -1) { break; }
        total += n;
        bytesleft -= n;
    }

    *len = total;

    return n==-1?-1:0;
}

int main(int argc , char *argv[])
{

    pthread_t client_thread;

    ip = argv[1];

    if (argc == 3)
        CLIENT_NO = 1;

    int arr_size = argc - 2; // new array size for ports
    int PORTS[arr_size];
    for (int i = 0; i < arr_size; i++){

        char *p;
        errno = 0;
        long conv = strtol(argv[i + 2], &p, 10);

        if (errno != 0 || *p != '\0' || conv > INT_MAX) {
            exit(EXIT_FAILURE);
        } else {
            PORTS[i] = conv;
        }
    }

    rep:
    printf("Enter message to send: \n");
	msg = read_line();


    for (int i = 0; i < arr_size; i++){

        if (pthread_create( &client_thread , NULL ,  connection_handler , (void*) PORTS[i]) < 0){
            perror("thread");
            exit(EXIT_FAILURE);
        }

    }

    //for more convenient output
    sleep(1);
    //pthread_join(client_thread, NULL);
    if (!CLIENT_NO){
        printf("New connection? (y/n)");

        char *ans;
        ans = read_line();
        if (strcmp("y",ans) == 0) {
            goto rep;
        }
    }

    pthread_exit(NULL);


    return 0;
}
void *connection_handler(void *threadid)
{

    //

    int cport = (uintptr_t)threadid;
    int sockfd;
    struct sockaddr_in serv_addr;
    char rbuff[MAX_BUFF_SIZE];

    #ifdef _WIN32
    WSADATA ws_data;
    if (WSAStartup(MAKEWORD(2, 0), &ws_data) != 0) {
        exit(1);
    }
    #endif

    if((sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0){
        printf("Failed creating socket\n");
        exit(EXIT_FAILURE);
    }

    bzero((char *) &serv_addr, sizeof (serv_addr));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_addr.s_addr = inet_addr(ip);
    serv_addr.sin_port = htons(cport);


    if (connect(sockfd, (struct sockaddr *) &serv_addr, sizeof (serv_addr)) < 0) {
        printf("Failed to connect to server\n");
    }




    //handling errors inside the function

    //if there is only one port we can send multiple messages

    cont:
    ;
    int len = strlen(msg);
    sendall(sockfd, msg, &len);

    int n = recv(sockfd, rbuff, MAX_BUFF_SIZE, 0);
    if (n == -1){
        perror("recv");
        exit(EXIT_FAILURE);
    }

    rbuff[n] = '\0';

    fputs(rbuff,stdout);
    fputs("\n",stdout);

    bzero(rbuff,MAX_BUFF_SIZE);

    if (CLIENT_NO){
        printf("New message? (y/n)");
        char *ans;
        ans = read_line();
        if (strcmp("y",ans) == 0) {
            printf("Enter new message: ");
            msg = read_line();
            goto cont;
        }
    }

    close(sockfd);

    return 0;
}

